use crate::stations::{StationsModel};
use crate::error_handler::CustomError;
use actix_web::{get, web, HttpResponse};

#[get("/stations")]
async fn find_all() -> Result<HttpResponse, CustomError> {
    let stations = StationsModel::find_all()?;
    Ok(HttpResponse::Ok().json(stations))
}

#[get("/stations/{id}")]
async fn find(id: web::Path<uuid::Uuid>) -> Result<HttpResponse, CustomError> {
    let station = StationsModel::find(id.into_inner())?;
    Ok(HttpResponse::Ok().json(station))
}

pub fn init_routes(config: &mut web::ServiceConfig) {
    config.service(find_all);
    config.service(find);
 }
