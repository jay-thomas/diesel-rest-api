use crate::error_handler::CustomError;
use actix_web::{get, web, HttpResponse};

#[get("/")]
async fn index() -> Result<HttpResponse, CustomError> {
    let html_string = include_str!("index.html");
    Ok(HttpResponse::Ok().body(html_string))
}

pub fn init_routes(config: &mut web::ServiceConfig) {
    config.service(index);
}
